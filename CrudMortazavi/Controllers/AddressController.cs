﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CrudMortazavi.DataAccess;
using CrudMortazavi.DataAccess.Models;
using CrudMortazavi.Services;

namespace CrudMortazavi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AddressController : ControllerBase
    {
        private readonly AddressService _addressService;

        public AddressController(AddressService addressService)
        {
            _addressService = addressService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Address>>> GetAddresss()
        {
            return await _addressService.GetAll();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Address>> GetAddress(int id)
        {
            var address = await _addressService.GetById(id);

            if (address == null)
                return NotFound();

            return address;
        }

        [HttpPut]
        public async Task<IActionResult> PutAddress(Address address)
        {
            var result = await _addressService.Update(address);

            if (result == null)
                return NotFound();

            return Ok(address);
        }

        [HttpPost]
        public async Task<ActionResult<Address>> PostAddress(Address address)
        {
            var result = await _addressService.Create(address);
            return result;
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Address>> DeleteAddress(int id)
        {
            var result = await _addressService.Delete(id);

            if (result == null)
                return NotFound();

            return Ok(result);
        }


    }
}
