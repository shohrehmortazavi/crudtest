﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CrudMortazavi.DataAccess;
using CrudMortazavi.DataAccess.Models;
using CrudMortazavi.Services;

namespace CrudMortazavi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonController : ControllerBase
    {
        private readonly PersonService _personService;

        public PersonController(PersonService personService)
        {
            _personService = personService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Person>>> GetPersons()
        {
            return await _personService.GetAll();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Person>> GetPerson(int id)
        {
            var person = await _personService.GetById(id);

            if (person == null)
                return NotFound();

            return person;
        }

        [HttpPut]
        public async Task<IActionResult> PutPerson(Person person)
        {
           
            var result = await _personService.Update(person);

            if (result == null)
                return NotFound();

            return Ok(person);
        }

        [HttpPost]
        public async Task<ActionResult<Person>> PostPerson(Person person)
        {
            var result = await _personService.Create(person);

            return result;
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Person>> DeletePerson(int id)
        {
            var result = await _personService.Delete(id);

            if (result == null)
                return NotFound();

            return Ok(result);
        }


    }
}
