﻿using CrudMortazavi.DataAccess.Models;
using Microsoft.EntityFrameworkCore;

namespace CrudMortazavi.DataAccess
{
    public class CrudMortazaviDbContext : DbContext
    {
        public CrudMortazaviDbContext(DbContextOptions options) : base(options)
        { }

        public DbSet<Person> Persons { get; set; }
        public DbSet<Address> Addresses { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
          
            builder.Entity<Person>().Property(p => p.FName).IsRequired().HasMaxLength(100);
            builder.Entity<Person>().Property(p => p.LName).IsRequired().HasMaxLength(100);
            builder.Entity<Person>().Property(p => p.NationalCode).IsRequired().HasMaxLength(20);

            builder.Entity<Address>().Property(p => p.AddressDesc).HasMaxLength(500);
            builder.Entity<Address>().Property(p => p.City).IsRequired().HasMaxLength(100);
            builder.Entity<Address>().Property(p => p.PostalCode).HasMaxLength(20);
    
            //Relations
            builder.Entity<Person>()
                   .HasMany(m => m.Addresses)
                   .WithOne(m => m.Person)
                   .IsRequired()
                   .OnDelete(DeleteBehavior.Cascade);
    }

    }
}
