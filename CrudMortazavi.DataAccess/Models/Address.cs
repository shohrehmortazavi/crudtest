﻿using System.ComponentModel.DataAnnotations.Schema;

namespace CrudMortazavi.DataAccess.Models
{
    public class Address
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; } 
        public string City { get; set; }
        public string AddressDesc { get; set; }
        public string PostalCode { get; set; }
        public virtual Person Person { get; set; }
    }
}