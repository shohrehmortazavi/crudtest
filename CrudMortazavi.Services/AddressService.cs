﻿using CrudMortazavi.DataAccess;
using CrudMortazavi.DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrudMortazavi.Services
{
    public class AddressService
    {
        private readonly CrudMortazaviDbContext _context;

        public AddressService(CrudMortazaviDbContext context)
        {
            _context = context;
        }

        public async Task<List<Address>> GetAll()
        {
            return await _context.Addresses.Include(a => a.Person).ToListAsync();
        }

        public async Task<Address> GetById(int addressId)
        {
           // var address = await _context.Addresses.Find(addressId);
            var address = await _context.Addresses.Include(a => a.Person).FirstOrDefaultAsync(a=>a.Id==addressId);
            if (address == null)
                throw new Exception("address not found!");
            return address;
        }

        public async Task<Address> Create(Address address)
        {
            if (address.Person == null || address.Person?.Id==0)
                throw new Exception("please select person");
            try
            {
                _context.Entry(address.Person).State = EntityState.Unchanged;
                _context.Addresses.Add(address);
                await _context.SaveChangesAsync();
                return address;
            }
            catch (Exception)
            {
                throw;
            }

        }

        public async Task<Address> Update(Address address)
        {
            if (address.Id == 0)
                throw new Exception("person not found! Id is 0;");

            _context.Entry(address).State = EntityState.Modified;
            _context.Update(address);
            try
            {
                await _context.SaveChangesAsync();
                return address;
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AddressExists(address.Id))
                    return null;
            }
            return address;
        }

        public async Task<Address> Delete(int addressId)
        {
            var address = await _context.Addresses.FindAsync(addressId);

            if (address == null)
                return null;
            _context.Addresses.Remove(address);
            await _context.SaveChangesAsync();
          
            return address;
        }

        private bool AddressExists(int id)
        {
            return _context.Addresses.Any(e => e.Id == id);
        }
    }
}
