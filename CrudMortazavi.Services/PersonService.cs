﻿using CrudMortazavi.DataAccess;
using CrudMortazavi.DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrudMortazavi.Services
{
    public class PersonService
    {
        private readonly CrudMortazaviDbContext _context;

        public PersonService(CrudMortazaviDbContext context)
        {
            _context = context;
        }

        public async Task<List<Person>> GetAll()
        {
            return await _context.Persons.Include(p => p.Addresses).ToListAsync();
        }

        public async Task<Person> GetById(int personId)
        {
           // var person = await _context.Persons.FindAsync(personId);
            var person = await _context.Persons.Include(p => p.Addresses).FirstOrDefaultAsync(a => a.Id == personId);

            if (person == null)
                throw new Exception("person not found!");
            return person;
        }

        public async Task<Person> Create(Person person)
        {

            try
            {
                _context.Persons.Add(person);
                await _context.SaveChangesAsync();
                return person;
            }
            catch (Exception)
            {
                throw;
            }

        }

        public async Task<Person> Update(Person person)
        {
            if (person.Id == 0)
                throw new Exception("person not found! Id is 0;");

            _context.Entry(person).State = EntityState.Modified;
            _context.Update(person);
            try
            {
                await _context.SaveChangesAsync();
                return person;
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PersonExists(person.Id))
                    return null;
            }
            return person;
        }

        public async Task<Person> Delete(int personId)
        {
            var person = await _context.Persons.FindAsync(personId);

            if (person == null)
                return null;
            _context.Persons.Remove(person);
            await _context.SaveChangesAsync();

            return person;
        }

        private bool PersonExists(int id)
        {
            return _context.Persons.Any(e => e.Id == id);
        }
    }
}
